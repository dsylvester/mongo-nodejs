"use strict";
var mongo = require('mongodb');
var connectString = "mongodb://localhost/test123";
console.log(mongo.MongoClient);
mongo.connect(connectString, function (err, db) {
    if (err) {
        console.log('Error: ' + err);
    }
    console.log(db);
    //db.test123();
    var names = db.collection('Names');
    names.insertOne({ "firstName": "Davis" });
    var namesList = ["Lancer", "Ashley", "Camille", "Collin", "Davis Jr", "Marilyn"];
    namesList.forEach(function (name) {
        names.insertOne({ "firstName": name }).then(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    });
    db.collections().then(function (data) {
        console.log('Collections: ' + JSON.stringify(data, null, 5));
        db.createCollection("Names", null).then(function (data) {
            console.log('Data: ' + JSON.stringify(data, null, 5));
        }, function (err) {
            console.log(err);
        });
    }, function (err) {
        console.log('Collection Error' + err);
    });
});
function collectionCallback() {
}
